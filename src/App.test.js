import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

// Needs to set license key on window to allow player to be created.
window.LICENSE_KEY = "123";

// Mock <apc-controls> to avoid problems with MutationObserver in Polymer.
jest.mock("@accurate-player/accurate-player-controls", () => () => (
  <apc-controls />
));

// Mock HTMLMediaElement.load to allow player initialization to work.
window.HTMLMediaElement.prototype.load = () => {
  /* do nothing */
};

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
