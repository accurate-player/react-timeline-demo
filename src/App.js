import React, {Component} from "react";
import "./App.css";
import {getMediaInfo} from "@accurate-player/probe";
import {ProgressivePlayer} from "@accurate-player/accurate-player-progressive";
import {
  HotkeyPlugin,
  DiscreteAudioPlugin,
  VttSubtitlePlugin,
  SmoothTimeUpdatePlugin,
  PointPlugin,
  SmoothTimeUpdateEventType
} from "@accurate-player/accurate-player-plugins";
import "@accurate-player/accurate-player-controls";
import {
  ApTimelineBasic,
  ApTimelineMarker,
  ApTimelineMetadata
} from "@accurate-player/accurate-player-components-external";
import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css"
import {secondsToFrames, secondsToTimecode, SeekMode, TimeFormat} from "@accurate-player/accurate-player-core";

ApTimelineBasic.register();
ApTimelineMarker.register();
ApTimelineMetadata.register();

const SERVER_URL = "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/";
const DEFAULT_AUDIO_TRACKS = [
  {
    track: {
      label: "Without voices",
      src: SERVER_URL + "aac-discrete/sintel-no-speak.mp4",
      channelCount: 2,
    },
    enabled: false,
  },
  {
    track: {
      label: "Stereo track",
      src: SERVER_URL + "aac-discrete/sintel_stereo.mp4",
      channelCount: 2,
    },
    enabled: false,
  },
  {
    track: {
      label: "Stereo with 5s offset",
      src: SERVER_URL + "aac-discrete/sintel_stereo_5s_offset.mp4",
      channelCount: 2,
      offset: -5,
    },
    enabled: false,
  },
  {
    track: {
      label: "Stereo with negative 5s offset",
      src: SERVER_URL + "aac-discrete/sintel_stereo_negative_5s_offset.mp4",
      channelCount: 2,
      offset: 5,
    },
    enabled: false,
  },
];

class App extends Component {
  videoElement;
  apControls;
  player;
  hotkeyPlugin;
  discreteAudioPlugin;
  vttSubtitlePlugin;
  pointPlugin;
  markerRow;
  markerMetadata;

  // Cache to store markers
  markers = [];

  // Current selected marker
  selectedMarker = undefined;

  // Current mouseover marker
  mouseoverMarker = undefined;

  componentDidMount() {
    this.player = new ProgressivePlayer(this.videoElement, window.LICENSE_KEY);
    // Load video file to player.
    getMediaInfo("https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/timecode/sintel-2048-timecode-stereo.mp4",
      {label: "Sintel Timecode", dropFrame: false}).then(file => {
      this.player.api.loadVideoFile(file);
    });
    // Initialize and register plugins to player.
    this.initPlugins();

    // Plugins are accessible through players api.
    // Register custom hotkey "m" to create markers.
    this.player.api.plugin.apHotkeyPlugin.setHotkey("m", () => {
      this.createMarker();
    });

    // Add hotkeys 'g' and 'h' to seek forward and backward 15 frames
    this.player.api.plugin.apHotkeyPlugin.setHotkey("g", () => {
      this.seekRelative(15);
    });
    this.player.api.plugin.apHotkeyPlugin.setHotkey("h", () => {
      this.seekRelative(-15);
    });

    // Initialize UI-controls.
    this.initControls();

    // Initialize marker callbacks
    this.initMarkerCallbacks();

    // Initialize callback for loadedmetadata event
    this.videoElement.addEventListener('loadedmetadata', (event) => {
      this.onLoadedMetadata(event);
    });

    document.getElementsByTagName("ap-timeline-basic")[0].player = this.player;
  }

  initPlugins() {
    this.hotkeyPlugin = new HotkeyPlugin(this.player);
    this.discreteAudioPlugin = new DiscreteAudioPlugin(this.player);
    this.vttSubtitlePlugin = new VttSubtitlePlugin(this.player);

    // Load discrete audio tracks
    this.discreteAudioPlugin.initDiscreteAudioTracks(DEFAULT_AUDIO_TRACKS);

    // Trigger time updates more frequent to create a smoother experience.
    this.smoothTimeUpdatePlugin = new SmoothTimeUpdatePlugin(this.player);

    // Add event listener to perform frequent updates.
    this.smoothTimeUpdatePlugin.on(SmoothTimeUpdateEventType.TimeUpdate,
      (event) => {
        // Update some visual element using the current frame value
        console.log(event.currentFrame);
      }
    );

    // Enable in/outpoint functionality.
    this.pointPlugin = new PointPlugin(this.player);

    // Set audio offset to 5 seconds
    this.setAudioOffset(SERVER_URL + "aac-discrete/sintel_stereo_negative_5s_offset.mp4", 10);

    // Increase audio offset with 1 second
    this.increaseAudioOffset(SERVER_URL + "aac-discrete/sintel_stereo_negative_5s_offset.mp4", 1);

    // Decrease audio offset with 1 second
    this.increaseAudioOffset(SERVER_URL + "aac-discrete/sintel_stereo_negative_5s_offset.mp4", -1);
  }

  // Increases the audio offset of a track, use negative values for negative offset
  increaseAudioOffset(src, seconds) {
    const currentOffset = Number(this.discreteAudioPlugin.getTrackOffset(src));
    const newOffset = Number(currentOffset + seconds);
    this.setAudioOffset(src, newOffset);
  }

  // Updates the offset of an audio track given by 'src' to 'seconds'
  setAudioOffset(src, seconds) {
    console.log('-> Setting audio offset to', seconds, src);
    console.log('Current offset', this.discreteAudioPlugin.getTrackOffset(src));

    // Update offset
    this.discreteAudioPlugin.setTrackOffset(src, seconds);
    console.log('New offset', this.discreteAudioPlugin.getTrackOffset(src));
  }



  initControls() {
    // Check if method exist, otherwise wait 100ms and try again.
    if (!this.apControls.init) {
      setTimeout(() => {
        this.initControls();
      }, 100);
      return;
    }

    // The Polymer 3.0 project is currently written in Javascript. Will be converted to TypeScript with typings shortly!
    this.apControls.init(this.videoElement, this.player, {
      saveLocalSettings: true // Enables storing of simple settings as volume, timeFormat and autoHide.
    });
  }

  // Initialize marker callbacks through the timeline metadata tag
  initMarkerCallbacks() {
    // Update the data input to the marker row.
    const metadata = this.markerMetadata;

    // Trigger callback when clicking on any marker
    metadata.apMarkerClick = ((marker) => {
      this.onMarkerClick(marker);
    });

    // This will enable selection of markers by clicking on them, which will trigger a special apSelectionChange callback
    metadata.select = true;
    metadata.selection = this.markers;
    metadata.apSelectionChange = ((marker) => {
      this.onMarkerSelect(marker);
    });

    // This will trigger when hovering over the marker
    metadata.apMarkerMouseEnter = ((marker) => {
      this.onMarkerMouseEnter(marker);
    });

    // This will trigger when leaving hover with marker
    metadata.apMarkerMouseLeave = ((marker) => {
      this.onMarkerMouseLeave(marker);
    });
  }

// Trigger on marker click, note that if selection is enabled, this will trigger both when selecting and deselecting
  onMarkerClick(marker) {
    console.log('Clicked on marker', marker)
  }

// Callback for marker selection
  onMarkerSelect(marker) {
    if (!marker.length) {
      console.log('Unselected marker');
      this.selectedMarker = undefined;
      return;
    }

    this.selectedMarker = marker[0];
    console.log('Selected marker', this.selectedMarker);

    // Seek to the start of the marker selected
    this.seekToFrame(Number(this.selectedMarker.start));
  }

  onMarkerMouseEnter(marker) {
    console.log('Mouse enter', marker);
    this.mouseoverMarker = marker;
  }

  onMarkerMouseLeave(marker) {
    console.log('Mouse leave', marker);
    this.mouseoverMarker = undefined;
  }

  createMarker() {
    // Only create marker if both in and outpoint is set
    if (this.pointPlugin.points.in && this.pointPlugin.points.out) {

      // Push the new marker to the manual markers array.
      this.markers.push({start: this.pointPlugin.points.in.frame, end: this.pointPlugin.points.out.frame});

      // Update the data input to the marker row.
      this.markerRow.data = [...this.markers];

      // Clear in and out points after creating the marker.
      this.pointPlugin.clearPoints();
    }
  }

  // Once loadedmetada event triggers, the duration of the video will be known
  onLoadedMetadata(event) {
    const duration = this.videoElement.duration;
    const timecode = secondsToTimecode(duration, 24, false);
    const frames = secondsToFrames(duration, 24);

    console.log('Loaded metadata => duration', duration, ', timecode', timecode, ', frames', frames);
  }

  // Seek to an exact frame
  seekToFrame(frame) {
    this.player.api.seek({
      format: TimeFormat.Frame,
      time: frame,
      mode: SeekMode.Absolute
    }).then(() => {
      // Seek complete
    });
  }

  // Seeks to a relative frame, meaning relative to the current position. Use negative values for backwards
  seekRelative(frames) {
    this.player.api.seek({
      format: TimeFormat.Frame,
      time: frames,
      mode: SeekMode.RelativeForward
    }).then(() => {
      // Seek complete
    });
  }

  render() {
    return (
      <div className="App">
        <div className="video-container">
          <video
            ref={ref => (this.videoElement = ref)}
            crossOrigin="true"
            playsInline={true}/>
          <apc-controls
            ref={ref => (this.apControls = ref)}/>
        </div>
        <ap-timeline-basic>
          <ap-timeline-row>
            <div slot="header">
              Markers
            </div>
            <ap-timeline-metadata ref={ref => (this.markerMetadata = ref)}>
              <ap-timeline-marker ref={ref => (this.markerRow = ref)}></ap-timeline-marker>
            </ap-timeline-metadata>
          </ap-timeline-row>
        </ap-timeline-basic>

        <div>
          <button onClick={() => this.pointPlugin.setInPoint()}>Set in point</button>
          <button onClick={() => this.pointPlugin.setOutPoint()}>Set out point</button>
          <button onClick={() => this.createMarker()}>Create marker</button>
        </div>
      </div>
    );
  }
}

export default App;
