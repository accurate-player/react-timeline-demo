# React Timeline Marker demo

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It demonstrates how to integrate Accurate Player within an React application and playing a statically hosted file using
the `ProgressivePlayer`. The `HotkeyPlugin` and the UI layer `<apc-controls>` is activated.

![Marker timeline view](app.png)

## Timeline

The timeline component from the UI component library is used below the player to visualize markers that are created.
To create a marker, scrub the timeline and set in and out points either using hotkeys (I, O) or by using the buttons
below the timeline. The buttons illustrate how to use external components to trigger API functions. Use the create
marker button (or hotkey M) once both points are set, the marker will now appear in the timeline.

Callback functions such as mouse click, selection, mouse enter, and mouse leave are integrated to illustrate how you
can use these to keep track of the current selected marker and the marker being hovered with the mouse. This is useful
if you want to conditionally show some tooltip or marker information, for example.

## Light theme

By default, a dark theme is used for Accurate Player and the timeline. Included in the repository is a light theme which overrides the CSS variables used by the UI component library. See the file `src/theme-light.css`.

If you'd like to use the light theme, simply uncomment the import line as shown below. It's important that the import comes after the App import line, to make sure it overrides the default variables.

From `src/index.js`:

```
// Uncomment line below to enable a light theme for timeline and components
import "./theme-light.css";
```

Screen shot of the light theme shown below.

![Light theme in timeline](light-theme.png)

## Setup

To setup the demos execute the following:

1. Login to Accurate Player npm repository to gain access to required dependencies, username and password will be provided to you by the Accurate Player team.
> npm login adduser --registry=https://codemill.jfrog.io/codemill/api/npm/accurate-video/ --scope=@accurate-player

2. Go to `shared/license-key-example.js` and follow the instructions of how to provide your license key correctly.

## Running the demo

1. `npm install`
2. `npm run start`
3. Open `http://localhost:3000` in your browser.

## Demo

https://demo.accurateplayer.com/marker-demo/

